import discord
from discord.ext import commands, tasks
from discord import Intents
import whois
import datetime
import asyncio

TOKEN = "DISCORD BOT TOKEN"

intents = discord.Intents.all()
intents.messages = True

# Bot init
bot = commands.Bot(command_prefix="!", intents=intents)

def get_expiry_date(domain):
    info = whois.whois(domain)
    if isinstance(info.expiration_date,list):
        expiry_date = info.expiration_date[0]
    else:
        expiry_date = info.expiration_date

    return expiry_date

def days_until_expiry(expiry_date):
    today = datetime.datetime.now()
    delta = expiry_date - today
    return delta.days

# Runs when Bot Succesfully Connects
@bot.event
async def on_ready():
    print(f'{bot.user} succesfully logged in!')

@bot.event
async def on_message(message):
    if message.author == bot.user:
        return

    if message.content == 'ping':
        await message.channel.send(f'Pong!')

    await bot.process_commands(message)

@tasks.loop(minutes=1)
async def check_expiry():
    now = datetime.datetime.now()
    if now.hour == 11 and now.minute == 0:
        domain_name = "yourdomain.com"
        expiry_date = get_expiry_date(domain_name)
        channel_id = Channel ID

        if expiry_date:
            days_remaining = days_until_expiry(expiry_date)

            if days_remaining <= 5:
                alert_message = f":rotating_light: Critical! {domain_name} will expire in {days_remaining} days."
            elif days_remaining <= 15:
                alert_message = f":bangbang: Urgent! {domain_name} will expire in {days_remaining} days."
            elif days_remaining <= 30:
                alert_message = f":exclamation: Warning! {domain_name} will expire in {days_remaining} days."
            else:
                alert_message = None

            if alert_message:
                channel = bot.get_channel(channel_id)
                await channel.send(alert_message)

@check_expiry.before_loop
async def before_check_expiry():
    await bot.wait_until_ready()

async def main():
    check_expiry.start()

asyncio.run(main())

@bot.command()
async def start_monitoring(ctx):
    check_expiry.start()
    await ctx.send("Monitoring Expiry Date has started.")

@bot.command()
async def stop_monitoring(ctx):
    check_expiry.stop()
    await ctx.send("Monitoring Expiry Date has stopped.")

bot.run(TOKEN)
